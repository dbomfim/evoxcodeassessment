import { firestore } from '../plugins/firebase'
import store from '../store/store'

const db = firestore.collection('todos')

const todoService = {
  list: async () => {
    const currentUser = store.getters['authentication/currentUser']
    const response = await db.where('owner', '==', currentUser.uid).orderBy('priority', 'desc').get()
    return response
  },

  subscribe: () => {
    // subscribe to changes on database
    const currentUser = store.getters['authentication/currentUser']
    db.where('owner', '==', currentUser.uid).onSnapshot(snapshot => {
      const changes = snapshot.docChanges()
      if (changes) {
        changes.forEach(change => {
          const newItem = {
            id: change.doc.id,
            data: change.doc.data()
          }
          return newItem
        })
      }
    })
  },

  create: async todo => {
    const response = await db.add(todo)
    if (response) {
      let newObj = {}
      newObj.id = response.id
      newObj.data = todo
      return newObj
    }

    return false
  },

  get: async () => {
    const response = await db.get()
    return response
  },

  search: async (user) => {
    // const response = await db.get()
    // return response
  },

  update: async (doc) => {
    const response = db.doc(doc.id).set(doc.data)
    return response
  },

  complete: async (id, status) => {
    const response = db.doc(id).update('status', status)
    return response
  },

  delete: async (docId) => {
    const response = db.doc(docId).delete()
    return response
  }
}

export default todoService
