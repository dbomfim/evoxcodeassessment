import firebase from 'firebase'

const userService = {
  login: async user => {
    const response = await firebase.auth().signInWithEmailAndPassword(user.username, user.password)
    return response
  },

  logout: async () => {
    const response = await firebase.auth().signOut()
    console.log(response)
    return response
  },

  register: async user => {
    const response = await firebase.auth().createUserWithEmailAndPassword(user.username, user.password)
    return response
  },

  refresh: async () => {
    const response = await firebase.auth().currentUser.getIdToken(true)
    return response
  },

  update: (userId, userProfile) => {
    return fetch(`/user/${userId}/profile`, {
      method: 'PUT',
      body: userProfile
    })
  },

  formatUserObject: response => {
    const { email, refreshToken, uid } = response.user
    return { email, refreshToken, uid }
  }
}

export { userService }
