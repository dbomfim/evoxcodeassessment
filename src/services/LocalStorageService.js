const localStorageService = {
  removeItem: key => window.localStorage.removeItem(key),

  setItem: (key, obj) => window.localStorage.setItem(key, JSON.stringify(obj)),

  getItem: key => {
    const obj = window.localStorage.getItem(key)
    if (obj) return JSON.parse(obj)
    return null
  },

  hasItem: key => !!window.localStorage.getItem(key),

  toJSON: () => JSON.stringify(window.localStorage)
}

export { localStorageService }
