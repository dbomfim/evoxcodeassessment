import Vue from 'vue'
import Vuelidate from 'vuelidate'

import './plugins/vuetify'
import { firebaseApp } from './plugins/firebase'

import App from './App.vue'

import { router } from './router'
import store from './store/store.js'
import './registerServiceWorker'

import { mapActions } from 'vuex'

Vue.config.productionTip = false

Vue.use(Vuelidate)

new Vue({
  data () {
    return {
      firebaseApp: null
    }
  },
  router,
  store,
  render: h => h(App),
  async created () {
    this.firebaseApp = firebaseApp
    await this.checkUserState()
  },
  methods: {
    ...mapActions('authentication', ['getFromLocalStorage']),
    checkUserState: async function () {
      await this.getFromLocalStorage()
    }
  }
}).$mount('#app')
