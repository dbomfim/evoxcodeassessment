import Vue from 'vue'
import Vuex from 'vuex'

// Modules
import authenticationStore from './authentication'
import todosStore from './todos'

Vue.use(Vuex)

const mainStore = new Vuex.Store({
  modules: {
    authentication: authenticationStore,
    todos: todosStore
  }
})

export default mainStore
