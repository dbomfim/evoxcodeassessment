import { userService } from '../services/UserService'
import { localStorageService } from '../services/LocalStorageService'

const authenticationStore = {
  namespaced: true,
  state: {
    authenticatedUser: null
  },
  mutations: {
    setAuthenticatedUser: (state, user) => {
      state.authenticatedUser = user
      localStorageService.setItem('authenticatedUser', user)
    },
    logoutUser: state => {
      state.authenticatedUser = null
      localStorageService.removeItem('authenticatedUser')
      console.log('user logged off!!')
    }
  },
  actions: {
    authenticate: async ({ commit }, credentials) => {
      let user = await userService.login(credentials)
      if (user) user = userService.formatUserObject(user)
      commit('setAuthenticatedUser', { ...user, ...credentials })
      return user
    },

    logout: async ({ commit }) => {
      commit('logoutUser')
    },

    getFromLocalStorage: ({ commit }) => {
      const user = localStorageService.getItem('authenticatedUser')
      if (user) commit('setAuthenticatedUser', user)
    },

    refreshCurrentUser: async ({ state, commit }, user) => {
      const refreshedUser = await userService.refresh()
      return refreshedUser
    }
  },
  getters: {
    isAuthenticated: state => state.authenticatedUser !== null,
    currentUser: state => state.authenticatedUser ? state.authenticatedUser : null
  }
}

export default authenticationStore
