import todoService from '../services/TodoService'

const todosStore = {
  namespaced: true,
  state: {
    filter: 'all',
    todos: [],
    selectedTodo: null
  },
  mutations: {
    setTodos: (state, todos) => {
      Object.assign(state.todos, todos)
    },
    addTodo: (state, todo) => {
      state.todos.unshift(todo)
    },
    delete: (state, id) => {
      state.todos = state.todos.filter(item => item.id !== id)
    },
    selectTodo: (state, todo) => {
      state.selectedTodo = todo
    },
    clearSelected: state => {
      state.selectedTodo = null
    },
    filterList: (state, filter) => {
      state.filter = filter
    },
    updateStateTodo: (state, todo) => {
      // in order to update a todo, just send on the object the propertis you want to change:
      // example: to change the status and priority, send an object like this { status: 'pending'. priority: 4 }
      state.todos.map(item => {
        if (item.id === todo.id) Object.assign(item.data, todo.data)
      })
    }
  },
  actions: {
    list: async ({ dispatch }, userUid) => {
      let todos = await todoService.list(userUid)
      if (!todos.empty) {
        todos = todos.docs
        todos = todos.map(item => {
          return {
            id: item.id,
            data: item.data()
          }
        })
        dispatch('orderList', todos)
      }
    },
    orderList: function ({ commit }, todos) {
      const todoOrderedList = todos.sort((a, b) => {
        return a.data.status === b.data.status
          ? b.data.priority - a.data.priority
          : a.data.status === 'done' ? 1 : -1
      })
      commit('setTodos', todoOrderedList)
    },
    addToList: ({ commit }, todo) => {
      todo.data.due_date = {
        seconds: Math.floor(new Date(todo.data.date) / 1000)
      }
      commit('addTodo', todo)
    },
    get: ({ commit }, item) => {
      commit('selectTodo', item)
    },
    delete: async ({ commit }, id) => {
      await todoService.delete(id)
      commit('delete', id)
    },
    complete: async ({ state, dispatch, commit }, todo) => {
      await todoService.complete(todo.id, todo.status)
      const value = { id: todo.id, data: { status: todo.status } }
      commit('updateStateTodo', value)
      dispatch('orderList', state.todos)
    },
    clear: ({ commit }) => {
      commit('clearSelected')
    },
    filter: ({ commit }, filter) => {
      commit('filterList', filter)
    },
    create: async ({ dispatch, commit }, todo) => {
      const response = await todoService.create(todo)

      if (response) dispatch('addToList', response)
      return response
    },
    update: async ({ dispatch, commit }, todo) => {
      console.log(todo)
      // const response = await todoService.update(todo)
      // console.log(response)
    }
  },
  getters: {
    selectedTodo: state => state.selectedTodo,
    listTodos: state => state.todos,
    filterBy: state => state.filter
  }
}

export default todosStore
