const UtilsMixin = {
  data () {
    return {
      showModal: false,
      model: ''
    }
  },
  watch: {
    model (val) {
      this.$emit('input', val)
    }
  },
  props: {
    value: String,
    label: String
  },
  methods: {
    hide () {
      this.showModal = false
    }
  },
  created () {
    this.$nextTick(function () {
      this.model = this.value
    })
  }
}

export { UtilsMixin }
