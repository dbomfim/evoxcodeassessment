import Vue from 'vue'
import Router from 'vue-router'

import store from './store/store'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '*',
      redirect: '/login'
    },
    {
      path: '/login',
      name: 'login',
      meta: {
        requiresAuth: false
      },
      component: () => import(/* webpackChunkName: "login" */ './views/Login.vue')
    },
    {
      path: '/home',
      name: 'home',
      meta: {
        requiresAuth: true
      },
      component: () => import(/* webpackChunkName: "home" */ './views/Home.vue')
    },
    {
      path: '/register',
      name: 'register',
      meta: {
        requiresAuth: false
      },
      component: () => import(/* webpackChunkName: "register" */ './views/Register.vue')
    }
  ]
})

router.beforeEach((to, from, next) => {
  const isAuthenticated = store.getters['authentication/isAuthenticated']

  const requiresAuth = to.matched.some(({ meta }) => {
    return meta.requiresAuth
  })

  if (requiresAuth) {
    if (isAuthenticated) {
      if (to.path !== from.path) store.dispatch('refreshCurrentUser')
      return next()
    }

    const { name, fullPath } = to
    const query = name !== undefined ? { redirect: name } : { path: fullPath }

    return next({ name: 'login', query })
  }

  return next()
})

export { router }
