import firebase from 'firebase'
import 'firebase/firestore'
import 'firebase/database'

const config = {
  apiKey: 'AIzaSyBLHunKDFbMIgYfjtF02aAUWFot__ID7-M',
  authDomain: 'evoxcodeassessment.firebaseapp.com',
  databaseURL: 'https://evoxcodeassessment.firebaseio.com',
  projectId: 'evoxcodeassessment',
  storageBucket: 'evoxcodeassessment.appspot.com',
  messagingSenderId: '611674887327',
  appId: '1:611674887327:web:c0e7e8878140cad3'
}

const firebaseApp = firebase.initializeApp(config)
const firestore = firebaseApp.firestore()
const firebaseReatimeDatabase = firebaseApp.database()

export { firebaseApp, firestore, firebaseReatimeDatabase }
