import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
// import '@fortawesome/fontawesome-free/css/all.css'

Vue.use(Vuetify, {
  theme: {
    primary: '#4169E1',
    secondary: '#4caf50',
    accent: '#2196f3',
    error: '#f44336',
    warning: '#ffeb3b',
    info: '#607d8b',
    success: '#8bc34a'
  },
  iconfont: 'md'
})
